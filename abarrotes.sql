CREATE TABLE `ventas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `producto` VARCHAR(45) NULL,
  `precio` VARCHAR(45) NULL,
  `cantidad` INT NULL,
  `importe` DECIMAL(10,2) NULL,
  PRIMARY KEY (`id`));


INSERT INTO `abarrotes`.`ventas` (`producto`, `precio`, `cantidad`, `importe`) VALUES ('ipad', '14000', '33','54324543.94');
INSERT INTO `abarrotes`.`ventas` (`producto`, `precio`, `cantidad`, `importe`) VALUES ('lap dell', '24000', '55','543543.66');
INSERT INTO `abarrotes`.`ventas` (`producto`, `precio`, `cantidad`, `importe`) VALUES ('lap toshiba', '22000', '99','500505.43');
